<?php
use Carbon\Carbon;

function setActiveRoute($name)
    {
        return request()->routeIs($name) ? 'active' : '';
    }

function setSubActiveRoute($name)
    {
        return request()->routeIs($name) ? 'class=navigation__active' : '';
    }

function setPriSubActiveRoute($name)
    {
        return request()->is($name) ? 'class=navigation__sub--active navigation__sub--toggled' : '';
    }

function diasHabilesXMes($mes, $anho){
    Carbon::setLocale('es');
    $diasFeriados = [
        '01-01', //Año nuevo
        '04-19', //Declaración de la Independencia,
        '05-01', //Día del trabajador
        '06-24', //Batalla de Carabobo
        '07-05', //Día de la independencia
        '07-24', //Natalicio de Simón Bolívar
        '10-12', //Día de la Resistencia Indígena
        '12-24', //Víspera de Navidad
        '12-25', //Navidad
        '12-31', //Fiesta de Fin de Año
    ];
    $fecha = date('d-m-Y', easter_date($anho)); //obtengo el domingo de semana santa o domingo de resurreccion
    $viernesSanto = date('m-d', strtotime($fecha."- 2 days")); //viernes santo (semana santa)
    $juevesSanto = date('m-d', strtotime($fecha."- 3 days")); //jueves santo (semana santa)
    $lunesCarnaval = date('m-d', strtotime($fecha."- 47 days"));//Martes de carnaval
    $martesCarnaval = date('m-d', strtotime($fecha."- 48 days"));//Lunes de carnaval
    array_push($diasFeriados, $viernesSanto);
    array_push($diasFeriados, $juevesSanto);
    array_push($diasFeriados, $lunesCarnaval);
    array_push($diasFeriados, $martesCarnaval);
    $numero = cal_days_in_month(CAL_GREGORIAN, $mes, $anho);
    $diasHabiles = array();
    for ($i=1; $i <= $numero ; $i++) { 
        $fecha = Carbon::createFromFormat("d-m-Y", "$i-$mes-$anho");
        $dia_mes = (string) $fecha->format('m-d');
        if ($fecha->format('l') !== 'Saturday' && $fecha->format('l') !== 'Sunday' && !in_array($dia_mes, $diasFeriados)) {
            array_push($diasHabiles, $fecha->format('d-M'));
        }
    }
    return $diasHabiles;
}

function formatoMes($mes,$anho)
{
    Carbon::setLocale('es');
    $fecha = Carbon::createFromFormat('m-Y', "$mes-$anho");
    return $fecha->format('M-Y');

}

function diccionario(){
    $diccionario = array(
        'viagra'=>5,
        'ofertas' => 4, 
        'buy' => 5, 
        'contactanos'=> 3,
        'tarifas' => 2, 
        'stock'=> 1, 
    );
    return $diccionario;
}


