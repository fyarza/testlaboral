<?php

namespace App\Http\Controllers\Messages;

use App\Exports\MessageExcel;
use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Http\Requests\Message\MessageRequest;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\MessageResource;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\Test;
use Illuminate\Http\Request;
use Excel;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //mensajes del Usuario que inicio session
        $usuario = Auth::user();
        $message = Message::where('subjectId',$usuario->id);
        if($request->has('ordeby')){
            $message->orderBy('date',(String)$request->input('ordeby')); 
        }
        return new MessageResource($message->get());
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Message\MessageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MessageRequest $request)
    {  
        $datos = $request->all();
        $user = Auth::user(); 
        $datos['subjectId'] = $user->id;
        $datos['toEmail'] = $user->email;
        $datos['date'] = Carbon::now();
        $palabras = diccionario();
        $body = $request->input('body');
        $suma = 0;
        $contador = 0;
        $promedio = 0;

        //Contador en Busqueda de Palabras del Dicionario
        foreach ($palabras as $key => $palabra) {
            $examinar = (String)$key;
            if(!empty($body) && substr_count($body,$examinar) > 0){
                $suma += $palabra;
                $contador += 1;
            }
        }

        //Evalua si encontro una palabra
        if($contador > 0)
            $promedio = $suma/count($palabras);
        
        $datos['spamScore'] = $promedio;
        //Crea el mensaje en la base de datos
        $message = Message::create($datos);
        //Envia el Correo sin no es spam
        if ($promedio < 2.5){
            Mail::to($message->fromEmail)
                ->send(new Test($message));
        }

        return (new MessageResource($message))
        ->response()
        ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Message\MessageRequest  $request
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(MessageRequest $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }

    public function export(Message $message) 
    {
        return Excel::download(new MessageExcel($message), 'message.xlsx');
    }
}
