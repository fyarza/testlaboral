<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';
    protected $guarded= ['id'];

       /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'subjectId' => 'string',
        'body' => 'string',
        'fromName' => 'string',
        'fromEmail' => 'string',
        'toEmail' => 'string',
        'asunto' =>'string',
        'date' =>'datetime:d/m/Y h:m:s',

    ];

    protected $dates =[
        'date',
        'created_at',
        'update_at'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'body' => 'required',
        'fromName' => 'required|max:191',
        'fromEmail' => 'required|email',
        'asunto' => 'required|in:reclamo,solicitud,queja'
    ];


    /**
     * 
     * 
     * @var array
     */

     public static $message =[
            'body.required' => 'El Cuerpo del Mensaje es Requerido.',
            'fromName.required' => 'El Nombre del destinatario es Requerido',
            'fromEmail.required' => 'El Email del Destinatario es Requerido',
            'fromEmail.email' => 'Ingrese un Email Valido',
            'asunto.required' =>  'El Asunto es Requerido',
            'asunto.in' => 'El Asunto solo puede ser reclamo, solicitud, queja'
     ];

     //Relacion con el Usuario que envia el mensaje
    public function subject(){
        return $this->belongsTo(User::class, 'subjectId','id');
    }
}
