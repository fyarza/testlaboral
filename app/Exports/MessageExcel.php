<?php

namespace App\Exports;

use App\Models\Message;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;

class MessageExcel implements FromView
{

    protected $message;

    /*Constructor para crear un objeto con valores*/
    public function __construct(Message $message)
    {
        $this->message = $message;
    }
    public function view(): View
    {
        $message = $this->message;
        return view('exports.reporteMessage', compact('message'));
    }
}
