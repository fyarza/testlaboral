<?php

use App\Mail\Test;
use App\Models\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|

| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'namespace' => 'Messages',
    'middleware' => 'auth'
], function () {
    Route::resource('messages','MessageController')->only('index','store');
    Route::get('/exportexcel/{message}','MessageController@export')->name('export.excel');
});


//Prueba de Ruta Email

Route::get('emailsend', function () {
    // Mail::to('federicoyarza295@gmail.com')
    //     ->send(new Test());
    $mensaje = Message::find(1);
    return new Test($mensaje);
});
