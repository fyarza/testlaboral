<style>
    .mt-5 {
        margin-top: 5rem;
    }
</style>
    <table class="mt-5">
        <thead>
            <tr>
                <td><b>Mensaje Enviado</b></td>
            </tr>
            <tr>
                <td colspan="3"><b>Fecha:&nbsp; </b>{{$message->date}} </td>
                <td colspan="3"><b>Asunto:&nbsp;</b> {{$message->asunto}} </td>
                <td colspan="6"><b>From Email:&nbsp;</b> {{$message->fromEmail}} </td>
            </tr>
            <tr></tr>
        </thead>
        <tbody>
            <!--Cabecera-->
            <tr>
                <td colspan="10"><b>Mensaje:&nbsp; </b><b>{{$message->body}}</b> </td>
            </tr>
            <tr>
                <td colspan="5"><b>spamScore:&nbsp; </b><b>{{$message->spamScore}}</b></td>
            </tr>
        </tbody>
    </table>