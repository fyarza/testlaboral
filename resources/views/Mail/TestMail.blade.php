@component('mail::message')
### Fecha de carga: {{$fecha}}
### Nombre: {{$message->fromName}}
### Email: {{$message->fromEmail}}
#### Asunto: {{$message->asunto}}
## Mensaje:
  {{$message->body}}

@endcomponent
