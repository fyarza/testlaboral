import React from 'react'
// import Swal from "sweetalert2";

const httpInstance = axios.create();

httpInstance.interceptors.response.use(null,error =>{
    const expectedError = error.response && error.response.status >= 400 && error.response.status < 500 ;

    if(expectedError){
        if(error.response.status == 422){
            // Swal.fire({
            //     icon: 'error',
            //     title: 'Oops...',
            //     text: 'Ocurrió un ' + JSON.stringify(error.response.data.errors),
            //     footer: 'Lo sentimos, vuelva a cargar la página'
            //   })
                  //Loggear mensaje de Error a un servicio como sentry
            //Mostrar error generico al usuario
            return Promise.reject(error.response.data.errors);
        }else{
            // Swal.fire({
            //     icon: 'error',
            //     title: 'Oops...',
            //     text: 'Ocurrió un ' + JSON.stringify(error.response.data),
            //     footer: 'Lo sentimos, vuelva a cargar la página'
            //   })
               //Loggear mensaje de Error a un servicio como sentry
            //Mostrar error generico al usuario
            return Promise.reject(error.response.data);
        }
       
       
    }
})
export default httpInstance;
