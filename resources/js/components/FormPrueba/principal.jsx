import * as React from 'react'
import FormMessaje from './components/FormMessaje'
import { ToastProvider, useToasts } from 'react-toast-notifications'
import http from '../../helpers/http.service'
import { Spinner } from 'react-bootstrap'
import { FaArrowsAltV, FaFilePdf, FaRegFileExcel } from 'react-icons/fa';
import Modal from './components/ModalEstadistica'


const Principal = () => {
    const [formCreate, setFormCreate] = React.useState(false);
    const [success, setSuccess] = React.useState(false);
    const [isLoading, setLoading] = React.useState(false);
    const [data, setData] = React.useState([]);
    const [order, setOrder] = React.useState(false);
    const [show, setShow] = React.useState(false)
    const { addToast } = useToasts();


    React.useEffect(() => {
        getData();
    }, [success])

    function getData(order = 'asc') {
        setLoading(true)
        http.get(`/messages?ordeby=${order}`)
            .then(response => {
                setData(response.data.data);
                setLoading(false);
            }).catch(error => {
                setLoading(false);
                addToast('Ocurrio un error!!', {
                    appearance: "error",
                    autoDismiss: true
                });

            });
    }

    const clickOrder = () => {
        if (order) {
            getData('desc')
            setOrder(false)
        } else {
            getData('asc')
            setOrder(true)
        }
    }

    const handleClick = () => {
        setFormCreate(!formCreate);
    }

    const handleExcel = (id) =>{
        
        const url = window.location.protocol +
        '//' +
        window.location.host + '/exportexcel/'+id
        window.open(url, '_blank')
    }

    const handlePdf = (id) => {
        const url = window.location.protocol +
        '//' +
        window.location.host + '/exportexcel/'+id
        console.log(url);
        //window.open(url, '_blank')
    }


    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <div className="d-flex justify-content-between align-items-center mb-2" role="group" aria-label="">
                <button type="button" className="btn btn-secondary" onClick={handleShow}>Estadística</button>
                <button type="button" className={formCreate ? "btn btn-danger" : "btn btn-primary"} onClick={handleClick} > {formCreate ? 'Regresar' : 'Redactar'}</button>
            </div>
            <Modal handleClose={handleClose} show={show} data={data} />
            {
                formCreate ?
                    <FormMessaje setFormCreate={setFormCreate} setSuccess={setSuccess} />
                    : <>
                        {
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Email </th>
                                        <th>Asunto</th>
                                        <th>Fecha <FaArrowsAltV onClick={clickOrder} style={{
                                            cursor: "pointer"
                                        }} /></th>
                                        <th>Spam</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        isLoading ? <tr align="center">
                                            <td colSpan={5} rowSpan={5}>
                                                <Spinner animation="grow" variant="info" >
                                                    <span className="sr-only">Loading...</span>
                                                </Spinner>
                                            </td>
                                        </tr>
                                            : <>
                                                {
                                                    data.length > 0 && data.map(item => (
                                                        <tr key={item.id}>
                                                            <td>{item.id}</td>
                                                            <td>{item.fromEmail} <FaFilePdf onClick={()=>{
                                                                handlePdf(item.id)
                                                            }} style={{
                                                                cursor: "pointer",
                                                                color:"red"
                                                            }} /> <FaRegFileExcel onClick={()=>{
                                                                handleExcel(item.id)
                                                            }} style={{
                                                                cursor: "pointer",
                                                                color:"green"
                                                            }}  /></td>
                                                            <td>{item.asunto}</td>
                                                            <td>{item.date}</td>
                                                            <td><span className="badge badge-primary">{item.spamScore}</span></td>
                                                        </tr>
                                                    ))
                                                }
                                            </>

                                    }


                                </tbody>
                            </table>
                        }

                    </>
            }


        </>
    )
}

const Inicio = () => (
    <ToastProvider>
        <Principal />
    </ToastProvider>
);

export default Inicio;
