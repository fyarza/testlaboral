import { Button, Modal } from 'react-bootstrap'
import * as React from 'react'

export default function ModalEstadistica({ show, handleClose, data }) {
  const [title, setTitle] = React.useState("");
  const [subtitle, setSubTitle] = React.useState("");
  const [list, setList] = React.useState([]);
  const reclamos = data.filter(item => (item.asunto == 'reclamo'));
  const solicitud = data.filter(item => (item.asunto == 'solicitud'));
  const queja = data.filter(item => (item.asunto == 'queja'));
  const handleReclamo = () => {
    setTitle("Reclamo");
    setSubTitle("Mensajes con Asunto de Tipo Reclamo")
    setList(reclamos);
  }
  const handleSolicitud = () => {
    setTitle("Solicitud");
    setSubTitle("Mensajes con Asunto de Tipo Solicitud")
    setList(solicitud);
  }
  const handleQueja = () => {
    setTitle("Queja");
    setSubTitle("Mensajes con Asunto de Tipo Queja")
    setList(queja);
  }
  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Estadistica de Mensaje</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <>
          <ul className="list-group">
            <li className="list-group-item d-flex justify-content-between align-items-center">
              Total de Mensajes Enviados
                  <span className="badge badge-success badge-pill">{data.length}</span>

            </li>
            <li className="list-group-item d-flex justify-content-between align-items-center">
              <a href="#" onClick={handleReclamo}>
                Mensajes con Asunto Reclamo
                 </a>
              <div className="d-flex">
                <small className="text-primary mr-2">{data.length > 0 ? parseFloat(parseInt(reclamos.length) * 100 / parseInt(data.length)).toFixed(2): 0}%</small>
                <span className="badge badge-danger badge-pill">{reclamos.length}</span>
              </div>
            </li>
            <li className="list-group-item d-flex justify-content-between align-items-center">
              <a href="#" onClick={handleSolicitud}>
                Mensajes con Asunto Solicitud
                 </a>
              <div className="d-flex">
                <small className="text-secondary mr-2">{ data.length > 0 ? parseFloat(parseInt(solicitud.length) * 100 / parseInt(data.length)).toFixed(2) : 0}%</small>
                <span className="badge badge-primary badge-pill">{solicitud.length}</span>
              </div>

            </li>
            <li className="list-group-item d-flex justify-content-between align-items-center">
              <a href="#" onClick={handleQueja}>
                Mensajes con Asunto Queja
                 </a>
              <div className="d-flex">
                <small className="text-primary mr-2">{data.length > 0 ?  parseFloat(parseInt(queja.length) * 100 / parseInt(data.length)).toFixed(2):0}%</small>
                <span className="badge badge-warning badge-pill">{queja.length}</span>
              </div>

            </li>
          </ul>
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">{title}</h4>
              <p className="card-text">{subtitle}</p>
            </div>
            <ul className="list-group list-group-flush">
              {
                list.map(item => {
                  return (<li key={item.id} className="list-group-item">
                    {item.fromEmail}
                    <br />
                    <small>{item.body}</small>

                  </li>
                  )
                })
              }
            </ul>
          </div>
        </>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Cerrar
            </Button>
      </Modal.Footer>
    </Modal>
  )
}
