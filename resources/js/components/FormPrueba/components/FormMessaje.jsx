import * as React from 'react'
import { Formik, Field, Form, ErrorMessage } from 'formik'
import * as Yup from 'yup'
import { useToasts } from 'react-toast-notifications';
import http from '../../../helpers/http.service'

export default function FormMessaje({setFormCreate,setSuccess}) {
    const { addToast } = useToasts();
    
    const initialValues = {
        body : '',
        fromName: '',
        fromEmail: '',
        asunto:'',
    }

    const validationSchema = Yup.object().shape({
        body: Yup.string().required('El Cuerpo del Mensaje es Requerido'),
        fromName: Yup.string().required('El Nombre del Destinatario es Requerido'),
        fromEmail: Yup.string().required('El Email del Destinatario es Requerido')
            .email('Ingrese un Email Valido'),
        asunto: Yup.string()
                    .required('El Asunto es Requerido')
                    .oneOf(['reclamo','solicitud','queja'],'El Asunto solo puede ser reclamo, solicitud, queja')


    });

    function onSubmit(fields, { setStatus, setSubmitting, resetForm,setErrors }) {
        setStatus();
        setSuccess(false);
        http.post('/messages', fields)
            .then(response => {
                setSubmitting(false);
                setFormCreate(false);
                setSuccess(true);
                addToast('Correo Enviado Exitosamente', {
                    appearance: "success",
                    autoDismiss: true
                });
                console.log(response);
            })
            .catch(error => {
                setSubmitting(false);
                setSuccess(false);
                setErrors(error)
                addToast('Ocurrio un error!!', {
                    appearance: "error",
                    autoDismiss: true
                });
                
            });
    }
    return (
        <Formik  initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
            {({ errors, touched, isSubmitting, handleChange,setFieldValue }) => (
            <Form>
                <div className="container">
                    <div className="form-row">
                        <div className="form-group col-12 col-md-6">
                            <label forhtml="fromName">Nombre<span>*</span></label>
                            <Field name="fromName" type="text"
                                className={'form-control ' + (errors.fromName && touched.fromName ? 'is-invalid' : '')}
                                placeholder="Ingrese el Nombre del Destinatario"
                            />
                            <ErrorMessage name="fromName" component="div" className="invalid-feedback" />
                        </div>
                        <div className="form-group col-12 col-md-6">
                            <label forhtml="fromEmail">Correo<span>*</span></label>
                            <Field name="fromEmail" type="email"
                                className={'form-control' + (errors.fromEmail && touched.fromEmail ? ' is-invalid' : '')}
                                placeholder="Ej:correo@dominio.com"
                            />
                            <ErrorMessage name="fromEmail" component="div" className="invalid-feedback" />
                        </div>
                        <div className="form-group col-12 col-md-6">
                            <label forhtml="asunto">Asunto<span>*</span></label>
                            <Field name="asunto" as="select"
                                    className={'form-control' + (errors.asunto && touched.asunto ? ' is-invalid' : '')}
                                >
                                    <option value="">Selecciones..</option>
                                    <option value="reclamo">Reclamo</option>
                                    <option value="solicitud">Solicitud</option>
                                    <option value="queja">Queja</option>
                                </Field>
                            <ErrorMessage name="asunto" component="div" className="invalid-feedback" />
                        </div>
                        <div className="form-group col-12 col-md-12">
                            <label forhtml="apellido">Mensaje<span>*</span></label>
                            <Field name="body" as="textarea"
                                    className={'form-control' + (errors.body && touched.body ? ' is-invalid' : '')}
                                 cols={60} rows={10}>
                                </Field>
                            <ErrorMessage name="body" component="div" className="invalid-feedback" />
                        </div>
                    
                    </div>
                   
                    <div className="col-12">
                        <div className="boton d-flex justify-content-between align-items-center">
                            <button type="submit" disabled={isSubmitting} className="btn btn-info btn-lg text-white">
                                Enviar
                                {isSubmitting && <i className="pi pi-spin pi-spinner" style={{ fontSize: '.8em', marginLeft: 5 }}></i>}
                            </button>
                        </div>
                    </div>
                </div>
            </Form>
        )
        }
    </Formik >
    )
}
