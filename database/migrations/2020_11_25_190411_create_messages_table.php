<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('subjectId');
            $table->foreign('subjectId')->references('id')->on('users')->onUpdate('cascade');
            $table->text('body');
            $table->string('fromName');
            $table->string('fromEmail');
            $table->string('toEmail');
            $table->enum('asunto',array('reclamo','solicitud','queja')); 
            $table->timestamp('date');
            $table->decimal('spamScore',18,2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
